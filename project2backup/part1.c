#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "bounded_queue.h"
#include "thread_safe_bounded_queue.h"
#include "utilities.h"
#include "topicqueue.h"

#define MAXSIZE 10
#define DEFAULT 64

struct TopicEntry{
	int entrynum;
	struct timeval timestamp;
	int pubID;
	char photoUrl[DEFAULT]; // URL TO PHOTO
	char photoCaption[DEFAULT]; // PHOTO CAPTION
};


void printQueue(struct topic_queue *queue){
	int i = 0;
	int itemcount = TQ_GetCount(queue);
	for (i = 0; i < itemcount; i++){
		fprintf(stderr, "Item #%d: %p\n",i,TQ_GetItem(queue,i));
	}
}

void emptyQueue(struct topic_queue *queue){
	int i = 0;
	int itemcount = TQ_GetCount(queue);
	for (i = 0; i < itemcount; i++){
		fprintf(stderr, "Item #%d: %d\n",i,TQ_TryDequeue(queue,TQ_GetBack(queue)));
	}
	fprintf(stderr, "Queue Count: %d\n",TQ_GetCount(queue));
	fprintf(stderr, "Queue is Empty: %d\n", TQ_IsEmpty(queue));
}

void runTest(struct FileLines* Lines){
	// Initialize testing variables
	void *item = NULL;
	int index = 0;  
	//struct FileLines *Destination = MakeEmptyLines(Lines->FileName,Lines->LineCount);
    //long long *ids = malloc(sizeof(long long) * Lines->LineCount);
    item = strdup(Lines->Lines[index++]);
	
	// Initialize the Queue
	fprintf(stderr, "Queue Initializing...\n");
	TopicQueue * myQueue = NULL;
	myQueue = TQ_MallocBoundedQueue(MAXSIZE);

	fprintf(stderr, "Queue is Empty: %d\n", TQ_IsEmpty(myQueue));
	fprintf(stderr, "Queue is Full: %d\n", TQ_IsFull(myQueue));
	fprintf(stderr, "Queue Count: %d\n",TQ_GetCount(myQueue));

	// Fill the Queue
	fprintf(stderr, "Filling the Queue...\n");
	int i = 0;
	for (i = 0; i < MAXSIZE-1; i++){
		TQ_TryEnqueue(myQueue, item);
	}
	fprintf(stderr, "Queue should be full\n");
	fprintf(stderr, "Queue is Empty: %d\n", TQ_IsEmpty(myQueue));
	fprintf(stderr, "Queue is Full: %d\n", TQ_IsFull(myQueue));
	fprintf(stderr, "Queue Count: %d\n",TQ_GetCount(myQueue));
	
	// Enqueue when full
	fprintf(stderr, "Attemping to enqueue when full. Should fail\n");
	TQ_TryEnqueue(myQueue, item);
	fprintf(stderr, "Queue Count: %d\n",TQ_GetCount(myQueue));

	fprintf(stderr, "Attempting to print all items in queue\n");
	printQueue(myQueue);

	fprintf(stderr, "Attempting to dequeue all items in queue. '1' indicates success\n");
	emptyQueue(myQueue);

	fprintf(stderr, "\n");

	free(item);
}

int main(int argc, char *argv[])
{
	for(int i=0; i< argc; i++){
		fprintf(stdout,"Argument %d:%s\n",i,argv[i]);
	}
    if(argc !=2 ){
        fprintf(stderr,"ERROR: invalid arguments. Usage: ./<prgm> <testfilename>\n");
        return -1;
    }
        
    struct FileLines* Lines = NULL;
        
    Lines = LoadAFile(argv[1]);
        
    TopicQueue *queue = NULL;
        
    queue = TQ_MallocBoundedQueue(Lines->LineCount);
        
    runTest(Lines);
        
    TQ_FreeBoundedQueue(queue);

    FreeFile(Lines);
    return 0;
}
