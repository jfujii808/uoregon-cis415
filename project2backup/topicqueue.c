#include <stdio.h>
#include <stdlib.h>
#include "bounded_queue.h"
#include "thread_safe_bounded_queue.h"
#include "topicqueue.h"

struct topic_queue{
	TSBoundedQueue *queue;
	long long lastid;
};

TopicQueue *TQ_MallocBoundedQueue(long size)
{
        struct topic_queue *returnValue = NULL;
        returnValue = (struct topic_queue*)malloc(sizeof(struct topic_queue));
        returnValue->queue = TS_BB_MallocBoundedQueue(size);
        return (TopicQueue *)returnValue; 
}

long long TQ_TryEnqueue(struct topic_queue *queue,void *item)
{
        long long returnValue = -1;
        returnValue = TS_BB_TryEnqueue(queue->queue,item);
        return returnValue;
}

int TQ_TryDequeue(struct topic_queue *queue,long long id)
{
        int  returnValue = 0;
        returnValue = TS_BB_TryDequeue(queue->queue,id);
        return returnValue;
}

long long TQ_GetFront(struct topic_queue *queue)
{
        long long returnValue = -1;
        returnValue = TS_BB_GetFront(queue->queue);
        return returnValue;
}

long long TQ_GetBack(struct topic_queue *queue)
{
        long long returnValue = -1;
        returnValue = TS_BB_GetBack(queue->queue);
        return returnValue;
}

int TQ_GetCount(struct topic_queue *queue)
{
        long long returnValue = 0;
        returnValue = TS_BB_GetCount(queue->queue);
        return (int)returnValue;
}

int TQ_IsIdValid(struct topic_queue *queue,long long id)
{
        int returnValue = 0;  
        returnValue = TS_BB_IsIdValid(queue->queue,id);
        return returnValue;
}

void *TQ_GetItem(struct topic_queue *queue,long long id)
{
        void *returnValue = NULL;
        returnValue = TS_BB_GetItem(queue->queue,id);
        queue->lastid = id;
        return returnValue;
}

int TQ_IsFull(struct topic_queue *queue)
{
        int returnValue = 0;
        returnValue = TS_BB_IsFull(queue->queue);
        return returnValue;
}

int TQ_IsEmpty(struct topic_queue *queue)
{
        int returnValue = 0;
        returnValue = TS_BB_IsEmpty(queue->queue);
        return returnValue;
}

void TQ_FreeBoundedQueue(struct topic_queue *queue)
{
        TS_BB_FreeBoundedQueue(queue->queue);
        queue->queue = NULL;
        free(queue);
}

