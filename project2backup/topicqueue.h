#ifndef TOPICQUEUE_H
#define TOPICQUEUE_H

typedef struct topic_queue TopicQueue;

TopicQueue *TQ_MallocBoundedQueue(long size);

long long TQ_TryEnqueue(TopicQueue *queue,void *item); 

int TQ_TryDequeue(TopicQueue *queue,long long id);

long long TQ_GetFront(TopicQueue *queue);

long long TQ_GetBack(TopicQueue *queue);

int TQ_GetCount(TopicQueue *queue);

int TQ_IsIdValid(TopicQueue *queue,long long id);

void *TQ_GetItem(TopicQueue *queue,long long id);

int TQ_IsFull(TopicQueue *queue);

int TQ_IsEmpty(TopicQueue *queue);

void TQ_FreeBoundedQueue(TopicQueue *queue);

#endif