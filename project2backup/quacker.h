#include <stdio.h>
#include <stdlib.h>

#define QUACKSIZE 128
#define MAXTOPICS 10
#define CAPTIONSIZE 64

struct topicentry{
	int entrynum;
	struct timeval timestamp;
	int pubID;
	char photoUrl[QUACKSIZE]; // URL TO PHOTO
	char photoCaption[CAPTIONSIZE]; // PHOTO CAPTION
}