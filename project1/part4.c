/* CIS 415 Project 1 Part 4
    Name: Jonathan Fujii
    DUCKID: 951472387
    Statement: All the bad code, broken functionality, and memory leaks in this code
    are my work. I've put cited reference code sources where necessary
    General References: 
        lab code, piazza code
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>

#define READY 0
#define RUN 1
#define PAUSE 2
#define EXIT 3
#define LINESIZE 128

sigset_t mask, oldmask;

int StartRun = 0;

struct Program
{
    char* command;
    char** cmdargs;
    int PID;
    int hasargs; //if 1 true, else 0
    int status;
};

/* PART 2 CHANGES */
/////////////////////////////////////////////////
void SignalHandler(int sig) {
    //https://www.thegeekstuff.com/2012/03/catch-signals-sample-c-code/
    if (sig == SIGUSR1){
        printf("RECEIVED SIGUSR1\n");
        StartRun = 1;
    }
}

void StartChildren(int programs, struct Program *programlist) {
    int programIndex = 0;
    for (programIndex = 0; programIndex < programs; programIndex++){
        fprintf(stderr, "Starting program: %s\n",programlist[programIndex].command);
        kill(programlist[programIndex].PID, SIGUSR1);
    }
}

void StopChild(struct Program program){
    kill(program.PID, SIGSTOP);
    fprintf(stderr, "Stopped program: %s\n",program.command);
    //program.status = PAUSE;
}

void StopChildren(int programs, struct Program *programlist){

    int programIndex = 0;
    for (programIndex = 0; programIndex < programs; programIndex++){
        StopChild(programlist[programIndex]);
    }
}

void ContinueChild(struct Program program){
    kill(program.PID, SIGCONT);
    program.status = RUN;
}

void ContinueChildren(int programs, struct Program *programlist){

    int programIndex = 0;
    for (programIndex = 0; programIndex < programs; programIndex++){
        ContinueChild(programlist[programIndex]);
    }
    fprintf(stderr, "SIGCONT\n");
}

/*
void WaitThenExec(struct Program program) {
        sigprocmask(SIG_BLOCK, &mask, &oldmask);

        // If we comment this block out at least the command will run at exit
        while (StartRun == 0) {
            fprintf(stderr,"StartRun is: %d\n",StartRun);
            sigsuspend(&oldmask);
        }
        sigprocmask(SIG_UNBLOCK, &mask, NULL);
        fprintf(stderr, "Program '%s' starting\n", program.command);
        program.status = RUN;
        // NEEDED IN CASE OF FORK BOMB
}
*/

/*void LaunchAllPrograms(int prognumber, struct Program *programs) {
    int curProg = 0;
    for (curProg = 0; curProg < prognumber; curProg++){
        fprintf(stderr,"Launching Program #: %d\n", curProg);

        int initPID = fork();
        programs[curProg].PID = initPID;
        if (initPID == 0){
            fprintf(stderr, "Launching: %s\n",programs[curProg].command);
            LaunchProgram(programs[curProg]);
        }
        if (initPID < 0){
            //ERROR
            fprintf(stderr,"Creation of child program unsuccessful\n");
        }
        if (initPID > 0){
            //PARENT
            fprintf(stderr,"Parent Process launched\n");
        }
        fprintf(stderr,"Launched Program #: %d\n", curProg);
        }
}*/

void LaunchAllPrograms(int prognumber, struct Program *programs) {
    int curProg = 0;
    for (curProg = 0; curProg < prognumber; curProg++){
        int initPID = fork();
        if (initPID != 0){
            //Parent Process
            fprintf(stderr,"Parent process launched\n");
            programs[curProg].PID = initPID;
        }
        else{
            // It's a child process
            fprintf(stderr,"Child process launched\n");
            while (StartRun == 0){
                usleep(100);
            }
            execvp(programs[curProg].command, programs[curProg].cmdargs);
            fprintf(stderr,"%s's execvp was unsuccessful\n", programs[curProg].command);
            //We need to exit to avoid fork bomb
            exit(-1);
        }
    }
}

void WaitProgram(struct Program program) {
    int childStatus;
    waitpid(program.PID, &childStatus, 0);
    fprintf(stderr, "Program '%s', id '%d'  exited with status '%d'\n", program.command, program.PID, childStatus);
}

void WaitPrograms(int lines, struct Program *programlist){
    int i = 0;
    for (i = 0; i < lines; i++){
        WaitProgram(programlist[i]);
    }
}

///////////////////////////////

int GetLines(FILE *file){
    // https://stackoverflow.com/questions/1910724/retrieving-total-line-numbers-in-a-file
    int lines = 0; // The amount of lines
    char line[LINESIZE];

    while(fgets(line, sizeof(line), file) != NULL){
        lines++;
    }

    fprintf(stderr, "Number of lines in file: '%d'\n", lines);
    fclose(file);
    return lines;
}
void PrintPrograms(int lines, struct Program *programlist){
    int i = 0;
    for (i = 0; i < lines; i++){
        fprintf(stderr, "Command: %s\n", programlist[i].command);
    }
}

void FreeProgram(struct Program program){
    // First check if there are args to free
    fprintf(stderr, "Attempting to TO FREE: %s\n", program.command);
    free(program.command);
    if (program.hasargs == 0){
        // This means there are no args
        return;
    }
    else{
        int argIndex = 0;
        while(program.cmdargs[argIndex] != NULL){
            free(program.cmdargs[argIndex]);
            argIndex++;
        }
    }
}
void FreePrograms(int programs, struct Program *programlist){
    fprintf(stderr, "TRYING TO FREE\n");
    int programIndex = 0;
    for (programIndex = 0; programIndex < programs; programIndex++){
        FreeProgram(programlist[programIndex]);
    }
    
}

void ReadFile(FILE *file, struct Program *programs){
    // https://stackoverflow.com/questions/3081289/how-to-read-a-line-from-a-text-file-in-c-c
    //Program* programlist = malloc(lines * sizeof *Program);
    char* ch;
    char line[1024];
    int i = 0;
    int curArgSize = 0;
    fprintf(stderr,"Initialized ReadFile values!\n");
    // For each line in the file
    while (fgets(line, sizeof(line), file)){
        //fprintf(stderr, "Moving onto line: %d\n", i+1);
        ch = strtok(line, " \n"); // This will be the command
        programs[i].command = strdup(ch); // ALLOCATES MEMORY, copies command into program
        fprintf(stderr, "command: %s\n",ch);
        char * buff = line;
        int args = 0;
        curArgSize = 0;

        while (*buff != '\0') { //While the buffer hasn't reached the end of the string
            if (*buff == ' ') {
                curArgSize++;
            }
            buff++;
        }
        //fprintf(stderr, "# of args: %d\n", curArgSize);

        programs[i].cmdargs = (char **)malloc(sizeof(char*) * (curArgSize+1)); // Assigns memory for args
        
        //char *initargs[curArgSize+1];
        //programs[i].cmdargs = initargs;
        int j = 0;
        ch = strtok(NULL, " \n"); //Gets the first arg
        if (ch != NULL){
            programs[i].cmdargs[j] = strdup(ch);
            args++;
        }
        while (ch != NULL){ 
        // While the line isn't finished, look for args
            programs[i].cmdargs[j] = strdup(ch);
            ch = strtok(NULL, " \n");
            //fprintf(stderr, "arg: %s\n", programs[i].cmdargs[j]);
            j++;
            args++;
        }
        if (ch == NULL){
            args--;
            programs[i].hasargs = 1;
            if (args == -1){
                // This mean the command has no args
                programs[i].hasargs = 0;
                args = 0;
            }
        }
        //fprintf(stderr, "Out of arg loop\n");
        programs[i].cmdargs[curArgSize+1] = NULL;
        fprintf(stderr, "total args #: %d\n", args);
        i++;
    }
}

int main(int argc, char *argv[]) {
    /*
    Takes the input file
    */

    // Checks for an input file
    FILE *input = NULL;


    if (argc == 2) {
        input = fopen(argv[1], "r");
        if (!input) {
            fprintf(stderr,"%s input file is not found! Exiting...\n", argv[1]);
            exit(1);
        }
        fprintf(stderr,"Got input!\n");
    }
    else{
        fprintf(stderr,"Invalid arguments! Exiting...\n");
        exit(1);
    }

    // Initialize other input variables
    if (input == NULL){
        fprintf(stderr, "File is empty\n");
    }
    fprintf(stderr,"Getting Lines!\n");
    int lines = GetLines(input);
    fprintf(stderr,"Got lines!\n");
    input = fopen(argv[1], "r");

    /* Set up the signals */
    signal(SIGUSR1, SignalHandler);

    struct Program programs[lines]; // Gets an array of programs
    fprintf(stderr,"Reading File!\n");
    ReadFile(input, programs);
    fprintf(stderr,"Launching Programs!\n");
    LaunchAllPrograms(lines, programs);
    fprintf(stderr,"Starting Children!\n");
    StartChildren(lines, programs);
    //fprintf(stderr,"Stopping Children!\n");
    StopChildren(lines, programs);
    //fprintf(stderr,"Continuing Children!\n");
    ContinueChildren(lines, programs);
    //fprintf(stderr,"Waiting Children!\n");
    WaitPrograms(lines, programs);
    //PrintPrograms(lines, programs); //MEMORY ERRORS HERE

    FreePrograms(lines, programs); //FIX ME

    fclose(input);

    return 0;
}