#ifndef COMPONENTS_H
#define COMPONENTS_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

struct Program
{
	char* command;
	char** cmdargs;
	int PID;
};

// Reads a file and returns the number of lines there are
int GetLines(FILE *file);

int GetArgs(char *line);

void PrintPrograms(int lines, struct Program **programlist);

// 
void WaitPrograms(struct Program **programlist);

// Reads a file and puts them into a list of Program structs
void ReadFile(FILE *file, struct Program **programlist);

void FreePrograms(struct Program **programlist)

void FreeProgram(struct Program *program)

// Takes a list of program structs and launches them
void LaunchPrograms(struct Program **programlist);

#endif