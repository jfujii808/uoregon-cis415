/* CIS 415 Project 1 Part 3
    Name: Jonathan Fujii
    DUCKID: 951472387
    Statement: All the bad code, broken functionality, and memory leaks in this code
    are my work. I've put cited reference code sources where necessary
    General References: 
        lab code, piazza code
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>

#define READY 0
#define RUN 1
#define PAUSE 2
#define EXIT 3
#define LINESIZE 128

sigset_t mask, oldmask;

int StartRun = 0;

/* PART 3 VARIABLES*/
struct Program *PCB = NULL; // Process control block
int ProgramCount = 0;
int CurrentProgram = 0;

enum state
{
    NotStarted,
    Running,
    Paused,
    Exited
};

struct Program
{
    char* command;
    char** cmdargs;
    int PID;
    int hasargs; //if 1 true, else 0
    int status;
    // PART 3 VARIABLES
    enum state State;
    int HasExited;
};

void StartChild(int sig) {
    //https://www.thegeekstuff.com/2012/03/catch-signals-sample-c-code/
    printf("RECEIVED StartChild\n");
    StartRun = 1;
}

void SignalHandler(int sig) {
    //https://www.thegeekstuff.com/2012/03/catch-signals-sample-c-code/
    if (sig == SIGUSR1){
        printf("RECEIVED SIGUSR1\n");
        StartRun = 1;
    }
}

void SigAlarmHandler(int sig){
    int i;
    int status;   
    // Loops over all the programs and checks if they are running
    for(i = 0; i < ProgramCount; i++){
        if(PCB[i].State == Running){
            // Wait on it to finish
            waitpid(PCB[i].PID , &status, WUNTRACED);
            if(PCB[i].State != Exited){
                // Pause it if it hasn't exited
                PCB[i].State = Paused;   
            }
            else{ // Update its state as Exited
                PCB[i].State = Exited;   
            }
        }
    } 

    // Checks programs if they are paused and iterates the CurrentProgram
    int ProgramIndex;
    for(ProgramIndex = 0; ProgramIndex < ProgramCount; ProgramIndex++){
        if(PCB[CurrentProgram].State == Paused){
            break;
        }
        CurrentProgram++;
        CurrentProgram = CurrentProgram % ProgramCount;
    }   
    // If the ProgramIndex is equal to the amount of programs, all of them have exited
    if(ProgramIndex == ProgramCount){
        printf("All processes are exited \n");
        return;
    }
    PCB[CurrentProgram].State = Running;
    kill(PCB[CurrentProgram].PID, SIGCONT);
    CurrentProgram++;
    CurrentProgram = CurrentProgram % ProgramCount;    
    alarm(2);
}

void StartChildren(int programs, struct Program *programlist) {
    int programIndex = 0;
    for (programIndex = 0; programIndex < programs; programIndex++){
        fprintf(stderr, "Starting program: %s\n",programlist[programIndex].command);
        kill(programlist[programIndex].PID, SIGUSR1);
    }
}

void StopChild(struct Program program){
    kill(program.PID, SIGSTOP);
    fprintf(stderr, "Stopped program: %s\n",program.command);
    //program.status = PAUSE;
}

void StopChildren(int programs, struct Program *programlist){

    int programIndex = 0;
    for (programIndex = 0; programIndex < programs; programIndex++){
        StopChild(programlist[programIndex]);
    }
}

void ContinueChild(struct Program program){
    kill(program.PID, SIGCONT);
    program.status = RUN;
}

void ContinueChildren(int programs, struct Program *programlist){

    int programIndex = 0;
    for (programIndex = 0; programIndex < programs; programIndex++){
        ContinueChild(programlist[programIndex]);
    }
    fprintf(stderr, "SIGCONT\n");
}

void LaunchAllPrograms(int prognumber, struct Program *programs) {
    int curProg = 0;
    for (curProg = 0; curProg < prognumber; curProg++){
        int initPID = fork();
        if (initPID != 0){
            //Parent Process
            fprintf(stderr,"Parent process launched\n");
            programs[curProg].PID = initPID;
        }
        else{
            // It's a child process
            fprintf(stderr,"Child process launched\n");
            while (StartRun == 0){
                usleep(100);
            }
            execvp(programs[curProg].command, programs[curProg].cmdargs);
            fprintf(stderr,"%s's execvp was unsuccessful\n", programs[curProg].command);
            //We need to exit to avoid fork bomb
            exit(-1);
        }
    }
}

void WaitProgram(struct Program program) {
    int childStatus;
    waitpid(program.PID, &childStatus, 0);
    fprintf(stderr, "Program '%s', id '%d'  exited with status '%d'\n", program.command, program.PID, childStatus);
}

void WaitPrograms(int lines, struct Program *programlist){
    int i = 0;
    for (i = 0; i < lines; i++){
        WaitProgram(programlist[i]);
    }
}

///////////////////////////////

int GetLines(FILE *file){
    // https://stackoverflow.com/questions/1910724/retrieving-total-line-numbers-in-a-file
    int lines = 0; // The amount of lines
    char line[LINESIZE];

    while(fgets(line, sizeof(line), file) != NULL){
        lines++;
    }

    fprintf(stderr, "Number of lines in file: '%d'\n", lines);
    fclose(file);
    return lines;
}
void PrintPrograms(int lines, struct Program *programlist){
    int i = 0;
    for (i = 0; i < lines; i++){
        fprintf(stderr, "Command: %s\n", programlist[i].command);
    }
}

void FreeProgram(struct Program program){
    // First check if there are args to free
    fprintf(stderr, "Attempting to TO FREE: %s\n", program.command);
    free(program.command);
    if (program.hasargs == 0){
        // This means there are no args
        return;
    }
    else{
        int argIndex = 0;
        while(program.cmdargs[argIndex] != NULL){
            free(program.cmdargs[argIndex]);
            argIndex++;
        }
    }
}
void FreePrograms(int programs, struct Program *programlist){
    fprintf(stderr, "TRYING TO FREE\n");
    int programIndex = 0;
    for (programIndex = 0; programIndex < programs; programIndex++){
        FreeProgram(programlist[programIndex]);
    }
    
}

void ReadFile(FILE *file, struct Program *programs){
    // https://stackoverflow.com/questions/3081289/how-to-read-a-line-from-a-text-file-in-c-c
    //Program* programlist = malloc(lines * sizeof *Program);
    char* ch;
    char line[1024];
    int i = 0;
    int curArgSize = 0;
    fprintf(stderr,"Initialized ReadFile values!\n");
    // For each line in the file
    while (fgets(line, sizeof(line), file)){
        //fprintf(stderr, "Moving onto line: %d\n", i+1);
        ch = strtok(line, " \n"); // This will be the command
        programs[i].command = strdup(ch); // ALLOCATES MEMORY, copies command into program
        fprintf(stderr, "command: %s\n",ch);
        char * buff = line;
        int args = 0;
        curArgSize = 0;

        while (*buff != '\0') { //While the buffer hasn't reached the end of the string
            if (*buff == ' ') {
                curArgSize++;
            }
            buff++;
        }
        //fprintf(stderr, "# of args: %d\n", curArgSize);

        programs[i].cmdargs = (char **)malloc(sizeof(char*) * (curArgSize+1)); // Assigns memory for args
        
        //char *initargs[curArgSize+1];
        //programs[i].cmdargs = initargs;
        int j = 0;
        ch = strtok(NULL, " \n"); //Gets the first arg
        if (ch != NULL){
            programs[i].cmdargs[j] = strdup(ch);
            args++;
        }
        while (ch != NULL){ 
        // While the line isn't finished, look for args
            programs[i].cmdargs[j] = strdup(ch);
            ch = strtok(NULL, " \n");
            //fprintf(stderr, "arg: %s\n", programs[i].cmdargs[j]);
            j++;
            args++;
        }
        if (ch == NULL){
            args--;
            programs[i].hasargs = 1;
            if (args == -1){
                // This mean the command has no args
                programs[i].hasargs = 0;
                args = 0;
            }
        }
        //fprintf(stderr, "Out of arg loop\n");
        programs[i].cmdargs[curArgSize+1] = NULL;
        fprintf(stderr, "total args #: %d\n", args);
        i++;
    }
}

int main(int argc, char *argv[]) {
    /*
    Takes the input file
    */

    // Checks for an input file
    FILE *input = NULL;


    if (argc == 2) {
        input = fopen(argv[1], "r");
        if (!input) {
            fprintf(stderr,"%s input file is not found! Exiting...\n", argv[1]);
            exit(1);
        }
        fprintf(stderr,"Got input!\n");
    }
    else{
        fprintf(stderr,"Invalid arguments! Exiting...\n");
        exit(1);
    }

    // Initialize other input variables
    if (input == NULL){
        fprintf(stderr, "File is empty\n");
    }
    fprintf(stderr,"Getting Lines!\n");
    int lines = GetLines(input);
    fprintf(stderr,"Got lines!\n");
    input = fopen(argv[1], "r");

    /* Set up the signals */
    signal(SIGALRM, SigAlarmHandler);

    struct Program programs[lines]; // Gets an array of programs
    PCB = programs;
    // Sets the ProgramCount to the # of lines in the file
    ProgramCount = lines;
    alarm(2); // Sets up an alarm

    fprintf(stderr,"Reading File!\n");
    ReadFile(input, programs);
    fprintf(stderr,"Launching Programs!\n");
    LaunchAllPrograms(lines, programs);
    fprintf(stderr,"Starting Children!\n");
    StartChildren(lines, programs);
    //fprintf(stderr,"Stopping Children!\n");
    StopChildren(lines, programs);
    //fprintf(stderr,"Continuing Children!\n");
    ContinueChildren(lines, programs);
    //fprintf(stderr,"Waiting Children!\n");
    WaitPrograms(lines, programs);
    //PrintPrograms(lines, programs); //MEMORY ERRORS HERE

    FreePrograms(lines, programs); //FIX ME

    fclose(input);

    return 0;
}