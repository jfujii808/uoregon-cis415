/* CIS 415 Project 2
    Name: Jonathan Fujii
    DuckID: 951472387
    Statement: This is my work with exception to lab and concept help from peers.
    References for whole assignment:
        Mostly course lab code.
    Note: Reference documentation also sometimes noted in function comment
    I also used a html format that was provided by another peer since the
    given html wasn't the greatest
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <ctype.h>
#include "p1fxns.h"
#include "project2.h"
#include "utilities.h"
#include "thread_safe_bounded_queue.h"

#define MAXSIZE 6
#define MAXLINELENGTH 128
#define MAXENTRIES 10000000
#define MAXQUEUES 1000
#define NUMPROXIES 6

// This is the global lock for testing threads
int cleanup_lock = 0;
int global_lock = 0;
int DELTA = 3;

struct TopicQueue {
    TSBoundedQueue *queue;
    int entry_counter;
    int ID;
    int length;
};

// Global variable for Topic Queue
int validIDS[MAXQUEUES] = {0};
TopicQueue queues[MAXQUEUES] = {{0}}; // Makes an array of TopicQueues of size MAXQUEUES
//struct TopicEntries entries[MAXENTRIES];


//https://stackoverflow.com/questions/7920793/how-to-add-a-character-at-end-of-string
char* stradd(const char* a, const char* b){
    size_t len = strlen(a) + strlen(b);
    char *ret = (char*)malloc(len * sizeof(char) + 1);
    *ret = '\0';
    return strcat(strcat(ret, a) ,b);
}

//======================== PART 2 CODE =============================

void *publisher_print(void *argument){
    char *temp = (char *)argument;
    char *text = (char *)argument;
    while(global_lock == 0){
        sleep(1);
    }
    // Load the File
    p1getword(text,0,temp);
    struct FileLines *lines = LoadAFile(text);
    if (lines == NULL){
        fprintf(stderr, "ERROR! File loaded improperly\n");
        return NULL;
    }


    int i;
    int ID;
    int linecount = lines->LineCount;
    char **filelines = lines->Lines;
    for (i = 0; i < linecount-1; i+=4){
        ID = atoi(filelines[i]);
        char *URL = filelines[i+1];
        char *Caption = filelines[i+2];
        int sleeptime = atoi(filelines[i+3]);
        
        // ADD A TOPIC ENTRY TO TOPIC QUEUE # ID
        // TOPIC ENTRY
        /*
        char *photoURL;
        char *photoCaption;
        int entrynum;
        int pubID;
        */
        struct TopicEntry entry = {URL, Caption,queues[ID].entry_counter,ID};
        enqueue(&queues[ID], entry);
        queues[ID].entry_counter++;

        sleep(sleeptime);
    }


    //FreeFile(&text);
    //FreeFile(lines);
    return NULL;
}

void *consumer_print(void *argument){
    char *temp = (char *)argument;
    char *text = (char *)argument;
    while(global_lock == 0){
        sleep(1);
    }
    // Load the File
    p1getword(text,0,temp);
    struct FileLines *lines = LoadAFile(text);
    if (lines == NULL){
        fprintf(stderr, "ERROR! File loaded improperly\n");
        return NULL;
    }
    int i;
    int waited = 0;
    int linecount = lines->LineCount;
    int lastread = 0;
    char **filelines = lines->Lines;

    FILE *writefile = fopen("my_subscriber_example_output.html","w");

    // Writes the html file start
    fprintf(writefile,"<!DOCTYPE html>\n<html>\n<head>\n");
    fprintf(writefile,"\t<title>InstaQuack</title>\n\t<style>\n");
    fprintf(writefile,"\t\t.a {border: 1px solid black; margin: 4px; display: inline-block}\n");
    fprintf(writefile,"\t\t.b {text-align: center}\n");
    fprintf(writefile,"\t\timg {width: 240px; height: auto}\n");
    fprintf(writefile,"\t\tp, h1, h2 {text-align: center}\n");
    fprintf(writefile,"\t</style>\n</head>\n<body>\n");
    fprintf(writefile,"\t<h1>InstaQuack (Subscriber)</h1>\n");

    for (i = 0; i < linecount; i++){
        // For every line
        waited = 0;
        int pubID = atoi(filelines[i]);
        struct TopicEntry printentry;
        // While we haven't reached Delta yet
        if (validIDS[pubID] == 0 || &queues[pubID] == NULL){
            // This means a Topic Queue with that ID hasn't been made yet
            continue;
        }

        while(waited != 1){
            waited = 1;
            // Get Entry
            sleep(DELTA);
            int check = getentry(&queues[pubID],lastread,&printentry);
            if (check != -1){
                fprintf(stderr, "Working on Topic %d with URL: %s\n",pubID,printentry.photoURL);
                fprintf(writefile,"\t<h2>Topic: %d</h2>\n\t<div class='b'>\n",pubID);
                fprintf(writefile,"\t\t<div class='a'>\n");
                fprintf(writefile,"\t\t\t<img src=%s>\n",printentry.photoURL);
                fprintf(writefile,"\t\t\t<p>%s</p>\n",printentry.photoCaption);
                fprintf(writefile,"\t\t</div>\n");
                fprintf(writefile,"\t</div>\n");
                lastread++;
                break;
            }
        }

    }
    fprintf(writefile,"</body>\n</html>");
    fclose(writefile);
    //FreeFile(lines);
    return NULL;
}

void *cleanup_print(void *argument){
    char *text = (char *)argument;
    while(cleanup_lock == 0){
        sleep(1);
    }
    fprintf(stderr,"STRING: %s\n", text);
    return NULL;
}

//======================== END OF PART 2 CODE =============================




//======================== START OF PART 1 CODE =============================


void initQueue(TopicQueue *queue, int size, int ID){
    // Initalizes the queue with the max size of size
    queue->queue = TS_BB_MallocBoundedQueue(size);
    queue->entry_counter = 0;
    queue->ID = ID;
    queue->length = size;
}

int enqueue(TopicQueue *queue, struct TopicEntry item){
    // We're enqueueing a new TopicEntry, we must malloc memory for it
    struct TopicEntry *itempointer = malloc(sizeof(struct TopicEntry));
    *itempointer = item;
    itempointer->entrynum = queue->entry_counter;
    // POTENTIALLY FIX LATER
    itempointer->pubID = queue->entry_counter;
    // Gets the timestamp
    gettimeofday(&(itempointer->timestamp), NULL);

    long long success = TS_BB_TryEnqueue(queue->queue, itempointer);

    if (success == -1){
        // If the queue is full we can't enqueue
        fprintf(stderr,"Enqueue has failed. Queue is full\n");
        free(itempointer);
        return -1;
    } else {
        queue->entry_counter += 1;
        return 1;
    }
}

int dequeue(TopicQueue *queue){
    struct TopicEntry *freeing = TS_BB_GetItem(queue->queue, TS_BB_GetBack(queue->queue));
    if (TS_BB_TryDequeue(queue->queue, TS_BB_GetBack(queue->queue)) != 1){
        fprintf(stderr,"There has been an error with dequeue\n");
        return -1;
    }
    free(freeing);
    return 1;
}

int getentry(TopicQueue *queue, long long lastentry, struct TopicEntry *entry){
    // Will return the ID of the newest entry
    if (TS_BB_IsEmpty(queue->queue) == 1){
        // If the queue is empty return -1
        return -1;
    } else if (TS_BB_IsIdValid(queue->queue, lastentry) == 1){
        // Else if the ID given is valid
        // We copy it to the entry pointer given to the function
        struct TopicEntry *temporary = TS_BB_GetItem(queue->queue, lastentry);
        *entry = *temporary;
        return lastentry;
    } else if (lastentry > TS_BB_GetFront(queue->queue)){
        // Else if the ID isn't valid and it's looking for something that
        // hasn't been made yet
        return -1;
    } else{
        // Else if the ID isn't valid but the queue isn't empty
        // We copy the tail entry into the given entry pointer
        long long tail = TS_BB_GetBack(queue->queue);
        struct TopicEntry *temporary = TS_BB_GetItem(queue->queue, tail);
        *entry = *temporary;
        return tail;
    }
}

long long print_queue(TopicQueue *queue){
    long long i, head = TS_BB_GetFront(queue->queue), tail = TS_BB_GetBack(queue->queue);
    for (i = tail; i < head; i++){
        struct TopicEntry *printitem = TS_BB_GetItem(queue->queue, i);
        fprintf(stderr,"Got Entry: %lld\n", i);
        fprintf(stderr,"\tID: %d\n", printitem->pubID);
        fprintf(stderr,"\tURL: %s\n", printitem->photoURL);
        fprintf(stderr,"\tCaption: %s\n", printitem->photoCaption);
        fprintf(stderr,"\tTimestamp: %ld\n", printitem->timestamp.tv_sec);
    }
    return i-1;
}

void print_entry(struct TopicEntry *printitem){
    fprintf(stderr,"Print Entry:\n");
    fprintf(stderr,"\tID: %d\n", printitem->pubID);
    fprintf(stderr,"\tURL: %s\n", printitem->photoURL);
    fprintf(stderr,"\tCaption: %s\n", printitem->photoCaption);
    fprintf(stderr,"\tTimestamp: %ld\n", printitem->timestamp.tv_sec);
}

void free_queue(TopicQueue *queue){
    while(TS_BB_IsEmpty(queue->queue) == 0){
        dequeue(queue);
    }
    TS_BB_FreeBoundedQueue(queue->queue);
}
int empty_queue(TopicQueue *queue){
    while(TS_BB_IsEmpty(queue->queue) == 0){
        dequeue(queue);
    }
    //TS_BB_FreeBoundedQueue(queue->queue);
    return 0;
}


void fill_queue(TopicQueue *queue, int type){
    int i;
    if (type == 1){
        for (i = 0; i < MAXSIZE-1; i++){
            struct TopicEntry entry = {"ABC", "XYZ"};
            enqueue(queue, entry);
        }
    }
    else{
        for (i = 0; i < MAXSIZE-1; i++){
            struct TopicEntry entry = {"CBA", "YXZ"};
            enqueue(queue, entry);
        }
    }
    
}

//======================== END OF PART 1 CODE =============================

int main(int argc, char* argv[]){
    // ============== CHECKING ARGUMENTS ================
    // https://www.programmingsimplified.com/c-program-read-file
    for(int i=0; i< argc; i++){
        //fprintf(stdout,"Argument %d:%s\n",i,argv[i]);
    }
    if(argc !=2 ){
        fprintf(stderr,"ERROR: invalid arguments. Usage: ./<prgm> <testfilename>\n");
        return -1;
    }

    FILE *fp; // File pointer
    fp = fopen(argv[1],"r");
    if (fp == NULL){
        printf("ERROR: opening file");
        // Program exits if file pointer returns NULL.
        exit(1);         
    }

    // =============== PART 3 COMMAND PARSING ===================

    pthread_t publisher[NUMPROXIES], consumer[NUMPROXIES], cleanup;
    char line[1024];
    char* ch;
    int queuecount = 0;
    int pcount = 0; // Producer count
    int scount = 0; // Subscriber count
    int pids[NUMPROXIES];
    int sids[NUMPROXIES];

    char* pubfiles[NUMPROXIES];
    char* subfiles[NUMPROXIES];

    fprintf(stderr,"\n===== READING THE FILE =====\n");
    while (fgets(line, sizeof(line), fp)){
        // While there are still lines,
        ch = strtok(line, " \n"); // This will be the command

        // ======= CREATE COMMAND =======
        if (strcmp(ch,"create") == 0){
            // Make a Topic Queue
            ch = strtok(NULL, " \n"); // IGNORE THIS VALUE
            ch = strtok(NULL, " \n"); // THIS IS THE ID
            int ID = atoi(ch);
            ch = strtok(NULL, " \n"); // ID NAME, IGNORE THIS FOR NOW
            ch = strtok(NULL, " \n"); // THIS IS THE QUEUE LENGTH
            int qlen = atoi(ch);
            if (queuecount < MAXQUEUES){ // If there is enough room to make a topic queue
                fprintf(stderr,"Making a Topic Queue with ID: %d Length: %d\n",ID,qlen);
                initQueue(&queues[ID],qlen,ID); // Makes a Topic Queue
                validIDS[ID] = 1;
                queuecount++;
            }
        }
        // ======= QUERY COMMAND =======
        else if (strcmp(ch,"query") == 0){
            ch = strtok(NULL, " \n"); // This can be: topics, publishers, or subscribers
            if (strcmp(ch,"topics") == 0){
                // PRINT TOPIC QUEUE IDS and LENGTHS
                fprintf(stderr,"Topic Queues:\n");
                int i = 0;
                for (i = 0; i < queuecount; i++){
                    fprintf(stderr, "\tID: %d | Length: %d\n", queues[i].ID+1, queues[i].length);
                }
                fprintf(stderr,"\n");
            }
            else if (strcmp(ch,"publishers") == 0){
                // PRINT publishers and command file names
                fprintf(stderr,"Publishers:\n");
                int i = 0;
                for (i = 0; i < pcount; i++){
                    fprintf(stderr, "\tPUB ID: %d | File: %s\n", pids[i], pubfiles[i]);
                }
                fprintf(stderr,"\n");
            }
            else if (strcmp(ch,"subscribers") == 0){
                // PRINT subscribers and command file names
                fprintf(stderr,"Subscribers:\n");
                int i = 0;
                for (i = 0; i < scount; i++){
                    fprintf(stderr, "\tSUB ID: %d | File: %s\n", sids[i], subfiles[i]);
                }
                fprintf(stderr,"\n");
            }
            else{
                fprintf(stderr,"INVALID COMMAND!\n");
            }
        }

        else if (strcmp(ch,"add") == 0){
            ch = strtok(NULL, " \n");
            if (strcmp(ch, "publishers") == 0){
                // Add publisher
                fprintf(stderr,"Added Publisher\n");
                ch = strtok(NULL, " \n"); // COMMAND FILE
                pubfiles[pcount] = strdup(ch);
                pthread_create(&publisher[pcount], NULL, publisher_print, pubfiles[pcount]);
                pids[pcount] = publisher[pcount];
                fprintf(stderr,"Pub ID: %d\n",pids[pcount]);
                pcount++;
            }
            else if (strcmp(ch, "subscribers") == 0){
                // Add subscriber
                fprintf(stderr,"Added Subscriber\n");
                ch = strtok(NULL, " \n"); // COMMAND FILE
                subfiles[scount] = strdup(ch);
                pthread_create(&consumer[scount], NULL, consumer_print, subfiles[scount]);
                sids[scount] = consumer[scount];
                scount++;
            }
            else{
                fprintf(stderr,"INVALID COMMAND!\n");
            }
        }
        else if (strcmp(ch, "start") == 0){
            fprintf(stderr, "=====WE ARE STARTING!=====\n\n");
            int i = 0;
            cleanup_lock = 1;
            global_lock = 1;
            pthread_create(&cleanup, NULL, cleanup_print, "CLEANUP THREAD ACTIVATED\n");
            pthread_join(cleanup, NULL);
            for (i = 0; i < pcount; i++){
                pthread_join(publisher[i], NULL);
            }
            for (i = 0; i < scount; i++){
                pthread_join(consumer[i], NULL);
            }
        }
        else if(strcmp(ch, "delta") == 0){
            ch = strtok(NULL, " \n");
            DELTA = atoi(ch);
            fprintf(stderr, "DELTA has been set to: %d\n\n",DELTA);
        }
        else{
            fprintf(stderr,"INVALID COMMAND!\n");
        }
        // ======THIS IS A DEBUG THING=====
        while (ch != NULL){ 
        // While the line isn't finished, look for args
            ch = strtok(NULL, " \n");
        }
    }
    int i = 0;
    for (i = 0; i< pcount; i++){
        free(pubfiles[i]);
    }
    for (i = 0; i< scount; i++){
        free(subfiles[i]);
    }



    //free_queue(&myQueue);
    fclose(fp);
    return 0;
}


