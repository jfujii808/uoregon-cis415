/* CIS 415 Project 2
	Name: Jonathan Fujii
	DuckID: 951472387
	Statement: This is my work with exception to lab and concept help from peers.
	References for whole assignment:
		Mostly course lab code.
	Note: Reference documentation also sometimes noted in function comment
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "project2.h"
#define NUMPROXIES 6

// This is the global lock for testing threads
int global_lock = 0;
int pidcount = 0;
int cidcount = 0;
int pids[NUMPROXIES];
int cids[NUMPROXIES];

void *producer_print(void *argument){
    char *text = (char *)argument;
    while(global_lock == 0){
        sleep(1);
    }
    fprintf(stderr,"ID: %d STRING: %s", pidcount,text);
    pidcount++;
    return NULL;
}

void *consumer_print(void *argument){
    char *text = (char *)argument;
    while(global_lock == 0){
        sleep(1);
    }
    fprintf(stderr,"ID: %d STRING: %s", cidcount,text);
    cidcount++;
    return NULL;
}

void *cleanup_print(void *argument){
    char *text = (char *)argument;
    while(global_lock == 0){
        sleep(1);
    }
    fprintf(stderr,"ID: %d STRING: %s",0,text);
    return NULL;
}

int main(int argc, char* argv[]){
    //https://stackoverflow.com/questions/6990888/c-how-to-create-thread-using-pthread-create-function
    pthread_t producer[NUMPROXIES], consumer[NUMPROXIES], cleanup;
    // pthread_create(pthread_t*, NULL, thread_function, thread_function_parameters)
    // Make Producer
    int i;
    fprintf(stderr,"Making %d Producer & Consumer threads\n", NUMPROXIES);
    for (i = 0; i < NUMPROXIES; i++){
        pids[i] = pthread_create(&producer[i], NULL, producer_print, "Unique Producer String\n");
    }
    // Make Consumer
    for (i = 0; i < NUMPROXIES; i++){
        cids[i] = pthread_create(&consumer[i], NULL, consumer_print, "Unique Consumer String\n");
    }
    // Make Cleanup
    pthread_create(&cleanup, NULL, cleanup_print, "Unique Cleanup Functionality\n");
    fprintf(stderr,"Initialized all of the threads\n");
    fprintf(stderr,"Releasing the floodgates\n");
    global_lock = 1;
    for (i = 0; i < NUMPROXIES; i++){
        pthread_join(producer[i], NULL);
        pthread_join(consumer[i], NULL);
    }
    pthread_join(cleanup, NULL);
    return 0;
}