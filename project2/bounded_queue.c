/* CIS 415 Project 2
        Name: Jonathan Fujii
        DuckID: 951472387
        Statement: This is my work with exception to lab and concept help from peers.
        NOTE: Of course the skeleton framework that this was built from isn't my work.
        References for whole assignment:
                Mostly course lab code.
        Note: Reference documentation also sometimes noted in function comment
*/

#include <stdio.h>
#include <stdlib.h>
#include "bounded_queue.h"

struct bounded_queue
{
        int size;       // maximum capacity of the queue
	// BIG NOTE!!! buffer index starts at 1
        void **buffer;  // storage
        long long head; // The empty space at the front of the queue  
        long long tail; // The end of the queue
};

int RoundIDToBufferIndex(int size, long long index)
{
        long long value = (index % ((long long)size));
        return (int)value;
}

BoundedQueue *BB_MallocBoundedQueue(long size)
{
        // https://www.techiedelight.com/circular-queue-implementation-c/
        struct bounded_queue *returnValue = malloc(sizeof(struct bounded_queue));

        // Initializes the queue's variables
        returnValue->buffer = malloc(size * sizeof(void*));
        returnValue->head = 0;
        returnValue->tail = 0;
        returnValue->size = size;
        return (BoundedQueue *)returnValue;
}

long long BB_TryEnqueue(struct bounded_queue *queue,void *item)
{
        // Will return -1 if failed to enqueue, and the head ID if successful
        long long returnValue = -1;
        if (BB_IsFull(queue)){
                // If the queue is fill, return -1 and a print statement for error
                //fprintf(stderr,"Queue is full! Error with Enqueue!\n");
                return returnValue;
        }
        else{
                // If the queue is not full it has room at the head
                // Set the head index's item to the new enqueued item
                int index = RoundIDToBufferIndex(queue->size, queue->head);
                queue->buffer[index] = item;
                returnValue = queue->head;
                // Move the head up a bit
                queue->head += 1;
        }
        return returnValue;
}

// FIXME
int BB_TryDequeue(struct bounded_queue *queue,long long id)
{
        // Returns 1 if it was successful, -1 if not
        int returnValue = -1;
        if (BB_IsEmpty(queue)){
                // Cannot dequeue something if the queue is empty
                //fprintf(stderr,"Queue is empty! Error with Dequeue\n");
                return returnValue;
        }
        else{
		// If the ID is the tail it's valid to dequeue!
		int index = RoundIDToBufferIndex(queue->size, id);
		queue->buffer[index] = NULL;
		queue->tail += 1;
		returnValue= 1;           
        }        
        return returnValue;
}

long long BB_GetFront(struct bounded_queue *queue)
{
        long long returnValue = queue->head;
        return returnValue;
}

long long BB_GetBack(struct bounded_queue *queue)
{
        long long returnValue = queue->tail;
        return returnValue;
}

int BB_GetCount(struct bounded_queue *queue)
{
        long long returnValue = (queue->head - queue->tail);
        
        return (int)returnValue;
}

int BB_IsIdValid(struct bounded_queue *queue,long long id)
{
        int returnValue = 1;
	//fprintf(stderr, "head: %ld, id: %ld, tail: %ld\n",queue->head,id,queue->tail);
        if (queue->head <= id || id < queue->tail){
                returnValue = 0;
		return returnValue;
        }
        return returnValue;
}

void *BB_GetItem(struct bounded_queue *queue,long long id)
{
        void *returnValue = NULL;
        if (BB_IsIdValid(queue, id) == 1){
                int index = RoundIDToBufferIndex(queue->size, id);
                returnValue = queue->buffer[index];
        }
        return returnValue;
}

int BB_IsFull(struct bounded_queue *queue)
{
        int returnValue = 0;
        // If the head - tail is equal to the size of the queue, it's full
	//fprintf(stderr, "queue count: %d | queue size: %d\n",BB_GetCount(queue),queue->size);
        if (BB_GetCount(queue) == queue->size-1){
                returnValue = 1;
        }
        return returnValue;
}

int BB_IsEmpty(struct bounded_queue *queue)
{       
        // If the head is the tail, it's empty
        int returnValue = 0;
        if (BB_GetCount(queue) == 0){
                returnValue = 1;
                return returnValue;
        }
        else{
                return returnValue;
        }
        
}

void BB_FreeBoundedQueue(struct bounded_queue *queue)
{
        free(queue->buffer);
	free(queue);
}

