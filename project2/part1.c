/* CIS 415 Project 2
	Name: Jonathan Fujii
	DuckID: 951472387
	Statement: This is my work with exception to lab and concept help from peers.
	References for whole assignment:
		Mostly course lab code.
	Note: Reference documentation also sometimes noted in function comment
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include "project2.h"
#include "thread_safe_bounded_queue.h"

#define MAXSIZE 6

struct TopicQueue {
    TSBoundedQueue *queue;
    long long entry_counter;
};

void initQueue(TopicQueue *queue, int size, int ID){
	// Initalizes the queue with the max size of size
    queue->queue = TS_BB_MallocBoundedQueue(size);
    queue->entry_counter = 0;
}

int enqueue(TopicQueue *queue, struct TopicEntry item){
	// We're enqueueing a new TopicEntry, we must malloc memory for it
    struct TopicEntry *itempointer = malloc(sizeof(struct TopicEntry));
    *itempointer = item;
    itempointer->entrynum = queue->entry_counter;
    // POTENTIALLY FIX LATER
    itempointer->pubID = queue->entry_counter;
    // Gets the timestamp
    gettimeofday(&(itempointer->timestamp), NULL);

    long long success = TS_BB_TryEnqueue(queue->queue, itempointer);

    if (success == -1){
    	// If the queue is full we can't enqueue
        fprintf(stderr,"Enqueue has failed. Queue is full\n");
        free(itempointer);
        return -1;
    } else {
        queue->entry_counter += 1;
        return 1;
    }
}

int dequeue(TopicQueue *queue){
    struct TopicEntry *freeing = TS_BB_GetItem(queue->queue, TS_BB_GetBack(queue->queue));
    if (TS_BB_TryDequeue(queue->queue, TS_BB_GetBack(queue->queue)) != 1){
        fprintf(stderr,"There has been an error with dequeue\n");
        return -1;
    }
    free(freeing);
    return 1;
}

int getentry(TopicQueue *queue, long long lastentry, struct TopicEntry *entry){
    // Will return the ID of the newest entry
    if (TS_BB_IsEmpty(queue->queue) == 1){
    	// If the queue is empty return -1
        return -1;
    } else if (TS_BB_IsIdValid(queue->queue, lastentry) == 1){
    	// Else if the ID given is valid and the next entry is valid
    	// We copy it to the entry pointer given to the function
        struct TopicEntry *temporary = TS_BB_GetItem(queue->queue, lastentry);
        *entry = *temporary;
        return lastentry+1;
    } else if (lastentry > TS_BB_GetFront(queue->queue)){
    	// Else if the ID isn't valid and it's looking for something that
    	// hasn't been made yet
    	fprintf(stderr, "This entry hasn't been posted yet\n");
    	return 0;
    } else{
    	// Else if the ID isn't valid but the queue isn't empty
    	// We copy the tail entry into the given entry pointer
    	long long tail = TS_BB_GetBack(queue->queue);
        struct TopicEntry *temporary = TS_BB_GetItem(queue->queue, tail);
        *entry = *temporary;
        return tail;
    }
    return 0;
}

long long print_queue(TopicQueue *queue){
    long long i, head = TS_BB_GetFront(queue->queue), tail = TS_BB_GetBack(queue->queue);
    for (i = tail; i < head; i++){
        struct TopicEntry *printitem = TS_BB_GetItem(queue->queue, i);
        fprintf(stderr,"Got Entry: %lld\n", i);
        fprintf(stderr,"\tID: %d\n", printitem->pubID);
        fprintf(stderr,"\tURL: %s\n", printitem->photoURL);
        fprintf(stderr,"\tCaption: %s\n", printitem->photoCaption);
        fprintf(stderr,"\tTimestamp: %ld\n", printitem->timestamp.tv_sec);
    }
    return i-1;
}

void print_entry(struct TopicEntry *printitem){
	fprintf(stderr,"Print Entry:\n");
    fprintf(stderr,"\tID: %d\n", printitem->pubID);
    fprintf(stderr,"\tURL: %s\n", printitem->photoURL);
    fprintf(stderr,"\tCaption: %s\n", printitem->photoCaption);
    fprintf(stderr,"\tTimestamp: %ld\n", printitem->timestamp.tv_sec);
}

void free_queue(TopicQueue *queue){
	while(TS_BB_IsEmpty(queue->queue) == 0){
        dequeue(queue);
    }
	TS_BB_FreeBoundedQueue(queue->queue);
}
int empty_queue(TopicQueue *queue){
    while(TS_BB_IsEmpty(queue->queue) == 0){
        dequeue(queue);
    }
    //TS_BB_FreeBoundedQueue(queue->queue);
    return 0;
}


void fill_queue(TopicQueue *queue, int type){
	int i;
	if (type == 1){
		for (i = 0; i < MAXSIZE-1; i++){
			struct TopicEntry entry = {"ABC", "XYZ"};
			enqueue(queue, entry);
		}
	}
	else{
		for (i = 0; i < MAXSIZE-1; i++){
			struct TopicEntry entry = {"CBA", "YXZ"};
			enqueue(queue, entry);
		}
	}
	
}

int main(int arc, char* argv[]){
    TopicQueue myQueue;
    initQueue(&myQueue, MAXSIZE, 0);
    fprintf(stderr,"Initialized Queue\n");

    fprintf(stderr,"Filling Queue to Max Capacity\n");

    fill_queue(&myQueue, 1);

    fprintf(stderr,"Current Queue Count: %d\n",TS_BB_GetCount(myQueue.queue));

    long long lastread = print_queue(&myQueue);

    struct TopicEntry entry = {"DEF", "ZYX"};
    fprintf(stderr, "Attempting to enqueue while Topic Queue is full\n");
    enqueue(&myQueue, entry);
    fprintf(stderr,"Current Queue Count: %d\n",TS_BB_GetCount(myQueue.queue));

    fprintf(stderr, "Emptying Topic Queue completely now\n");
    empty_queue(&myQueue);
    fprintf(stderr, "Queue should now be empty\n");
    fprintf(stderr,"Current Queue Count: %d\n",TS_BB_GetCount(myQueue.queue));

    fprintf(stderr,"Filling Queue to Max Capacity again with new entries\n");

    fill_queue(&myQueue, 2);

    fprintf(stderr,"Dequeueing 2 entries from the queue\n");
    dequeue(&myQueue);
    dequeue(&myQueue);

    //lastread = print_queue(&myQueue);

    fprintf(stderr,"Attempting to get the next entry after the last read entry: %lld\n",lastread);
    struct TopicEntry printitem = {"",""};
    lastread = getentry(&myQueue, lastread++, &printitem);
    print_entry(&printitem);
    fprintf(stderr,"It should have defaulted to Entry 7 (The Tail)\n");
    fprintf(stderr,"Attempting to get the  next entry after the last read entry: %lld\n",lastread++);
    getentry(&myQueue, lastread, &printitem);
    print_entry(&printitem);
    //lastread = print_queue(&myQueue);
    lastread++;
    lastread++;
    lastread++;

    fprintf(stderr,"Read an entry that hasn't been made yet at ID: %lld while head is at %lld:\n",lastread,TS_BB_GetFront(myQueue.queue));

    getentry(&myQueue, lastread, &printitem);

    free_queue(&myQueue);
    return 0;
}

