/* CIS 415 Project 2
	Name: Jonathan Fujii
	DuckID: 951472387
	Statement: This is my work with exception to lab and concept help from peers.
	References for whole assignment:
		Mostly course lab code.
	Note: Reference documentation also sometimes noted in function comment
*/

#ifndef PROJECT2_H
#define PROJECT2_H
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/time.h>

typedef struct TopicQueue TopicQueue;

struct TopicEntry {
    char *photoURL;
    char *photoCaption;
    int entrynum;
    int pubID;
    struct timeval timestamp;
};

void initQueue(TopicQueue *queue, int size, int ID);

int enqueue(TopicQueue *queue, struct TopicEntry item);

int dequeue(TopicQueue *queue);

int getentry(TopicQueue *queue, long long ID, struct TopicEntry *entry);

int empty_queue(TopicQueue *queue);

void *producer_print(void *arg);

void *consumer_print(void *arg);

void *cleanup_print(void *arg);






#endif