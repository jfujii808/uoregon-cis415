/* CIS 415 Project 0 main.c
	Name: Jonathan Fujii
	DuckID: 951472387
	Statement: This is my work with exception to reference code for algorithms and command usage.
	References for whole assignment:
		iteration clarification - https://stackoverflow.com/questions/22269435/how-to-iterate-through-a-list-of-objects-in-c
		course lab code. lab 0, lab 1
	Note: Reference documentation also sometimes noted in function comment
*/

#include "anagram.h"


int main(int argc, char *argv[])
{
	// Reference to Lab Code Week 2
	// http://www.cplusplus.com/reference/cstdio/stdin/
	FILE *in = NULL;
	FILE *out = NULL;
	in = stdin;
	out = stdout;
	struct AnagramList *alist = NULL;
	char words[1024];
	char fromStream;
	int i = 0;

	if (argc > 3 || argc < 1){
		printf("Invalid number of arguments! Exiting...\n");
		exit(1);
	}
	// INPUT CASE
	if (argc == 3 || argc == 2){
		in = fopen(argv[1], "r");
		if (!in) {
            printf("%s is not found! Exiting...\n", argv[1]);
            exit(1);
        }
	}
	else{
		in = stdin;
	}
	// OUTPUT CASE
	if (argc == 3){
		out = fopen(argv[2], "w");
	}
	else{
		out = stdout;
	}
	while ((fromStream = getc(in)) != EOF){
		if (fromStream == '\n'){
			words[i] = '\0';
			i = 0;
			AddWordAList(&alist, words);
		}
		else{
			words[i] = fromStream;
			i += 1;
		}
	}
	PrintAList(out, alist);
	FreeAList(&alist);
	fclose(in);
	fclose(out);
	return 0;
}
