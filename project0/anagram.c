/* CIS 415 Project 0 anagram.c
	Name: Jonathan Fujii
	DuckID: 951472387
	Statement: This is my work with exception to reference code for algorithms and command usage.
	References for whole assignment:
		iteration clarification - https://stackoverflow.com/questions/22269435/how-to-iterate-through-a-list-of-objects-in-c
		course lab code. lab 0, lab 1
	Note: Reference documentation also sometimes noted in function comment
*/


#include "anagram.h"

char *LowercaseConvert(char *copy, char *word);
void QuickSort(char * word, int low, int high);
int Partition(char * word, int low, int high);
void QuickSort(char * word, int low, int high) {
	// referenced from https://www.geeksforgeeks.org/quick-sort/
    if (low < high) {
        int pi = Partition(word, low, high);
        QuickSort(word, low, pi - 1);
        QuickSort(word, pi + 1, high);
    }
}
int Partition(char * word, int low, int high) {
	// referenced from https://www.geeksforgeeks.org/quick-sort/
    char pivot = word[high]; // pivot
    int i = low - 1; // index of smaller element
    int j;
    for (j = low; j < high; j++) {
        if (word[j] < pivot) {
            i++;
            char toSave = word[j];
            word[j] = word[i];
            word[i] = toSave;            
        }
    }
    char temp = word[high];
    word[high] = word[i + 1];
    word[i + 1] = temp;
    return (i + 1);	
}
char *LowercaseConvert(char * word, char * copy){
	// Converts items in word to lowercase in copy
	int i;
	for (i = 0; word[i]; i++){
		copy[i] = tolower(word[i]);
	}
return 0;
}

// Sets things up for a new StringList struct
struct StringList *MallocSList(char *word){
	struct StringList *returnlist = (struct StringList*)malloc(sizeof(struct StringList)); //mallocs the memory

	// =================== StringList Component Setup ===================
	// Word Component
	int wordlen = strlen(word) + 1; // Is the length of the word with room for the null char at end
	returnlist->Word = (char *)malloc(sizeof(char)*wordlen); // Sets memory for the word
	// Next Component
	returnlist->Next = NULL; // Initializes the Next value as NULL
	strcpy(returnlist->Word, word);
	

	return returnlist;
}

// Appends a stringlist node to a stringlist
void AppendSList(struct StringList **head, struct StringList *node){
	if (*head == NULL){
		// If the head pointer is null, we don't have to check for anything else.
		*head = node;
	}
	else{
		struct StringList * cnode = *head; //cnode = current node
		// Iterates through the string list until a null pointer found
		// Utilizes the Next pointer already in the StringList struct
		while (cnode->Next != NULL){
			cnode = cnode->Next;
		}
		// Executes this once it finds the null pointer
		cnode->Next = node;
	}
}

void FreeSList(struct StringList **node){
	// If the node pointer is null, return
	if ((*node) == NULL){
		return;
	}
	// Otherwise, initialize steps
	struct StringList *cnode = *node; //cnode = current node
	struct Stringlist *nnode = NULL; //nnode = next node
	// Needs to do this at least free something
	while(cnode != NULL) {
		nnode = cnode->Next;
		free(cnode->Word);
		free(cnode);
		cnode = nnode;
	}
	
}

void PrintSList(FILE *file,struct StringList *node){
	if (node == NULL){
		// If the node is null, nothing to print
		return;
	}
	else{
		struct StringList * cnode = node;
		while (cnode != NULL){
			fprintf(file, "\t%s\n", cnode->Word);
			cnode = cnode->Next;
		}
	}



}

int SListCount(struct StringList *node){
	int scount = 0; // amount of strings
	struct StringList *cnode = node;
	while (cnode != NULL){
		scount++;
		cnode = cnode->Next;
	}
	return scount;
}

struct AnagramList* MallocAList(char *word){
	struct AnagramList *alist = (struct AnagramList*)malloc(sizeof(struct AnagramList));
	// =================== AnagramList Component Setup ===================
	// Anagram Component
	// Next Component
	alist->Next = NULL;
	int wordlen = strlen(word) + 1; // Word length
	alist->Anagram = (char*)malloc((wordlen * sizeof(char)));
	strcpy(alist->Anagram, word);
	LowercaseConvert(word, alist->Anagram);
	int high = strlen(word - 1);
	QuickSort(alist->Anagram, 0, high);

	// Word Component
	alist->Words = MallocSList(word);

	

	return alist;

}

void FreeAList(struct AnagramList **node){
	if ((*node) == NULL){
		return;
	}
	struct AnagramList *cnode = *node;
	struct AnagramList *nnode = NULL;
	do {
		nnode = cnode->Next;
		free(cnode->Anagram);
		FreeSList(&cnode->Words);
		free(cnode);
		cnode = nnode;
	} while(cnode != NULL);
}

void PrintAList(FILE *file,struct AnagramList *node){
	if (node == NULL){
		// If the node is null, nothing to print
		return;
	}
	struct AnagramList * cnode = node;
	int wcount = SListCount(cnode->Words);
	while (cnode != NULL){
		wcount = SListCount(cnode->Words);
		if (wcount > 1){
			// No 1 word anagrams
			fprintf(file, "%s:%d\n", cnode->Anagram, wcount);
			PrintSList(file, cnode->Words);
		}
		cnode = cnode->Next;
		
	}
}

void AddWordAList(struct AnagramList **node, char *word){
	// http://www.c4learn.com/c-programming/c-reference/memset-function/
	// http://www.cplusplus.com/reference/cstring/strcmp/
	if (*node == NULL){
		*node = MallocAList(word);
		return;
	}
	char search[1024];
	memset(search, '\b', sizeof(search));
	strcpy(search,word);
	LowercaseConvert(word, search);
	QuickSort(search, 0, strlen(search)-1);

	if (*node == NULL) {
		*node = MallocAList(word);
		return;
	}
	while (*node != NULL )
	{
		if (strcmp((**node).Anagram,search) == 0) {	
			struct StringList *thisNode = MallocSList(word);
			AppendSList(&(*node)->Words, thisNode);
			break;
		} else {
			if ((*node)->Next == NULL) {
				(*node)->Next = MallocAList(word);
				return;
			} else {
				node = &(*node)->Next;
			}
		}
	}
}
