CIS 415 Project 0 report.txt
Name: Jonathan Fuji
DuckID: 951472387
Current State:
Right now when my program is run there should be NO memory leaks or memory errors in Valgrind.
It runs from start to finish and outputs anagram lists in the specified format.

HOWEVER:
The outputted anagram list is only partial and is not exhaustive or complete.
There are 2 compile warnings about incompatible pointer assignment.
